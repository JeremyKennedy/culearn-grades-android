# CuLearn Grades
Checking grades on CuLearn is tedious – you have to log in, select a course, scroll to the grades link, click it, and repeat that for each course you want to check. When you’re waiting to receive multiple marks, this becomes repetitive very quickly. I made an app that fixes that, and makes checking your grades quick and easy.

CuLearn Grades is an app designed to help students check their grades as fast as possible. Its main function is to read grade information from CuLearn, and display that data to the user in an easy-to-read format.

# Implementation

![](documentation/architecture.png)

This system has two main components, the API server and the Android app. Simply put, the API handles all the difficult data manipulation and logic with cuLearn, then sends it to the Android app, which purely presents the information to the user.

## API Server
**Link to API repository**: https://gitlab.com/JeremyKennedy/culearn-grades-api

The API server is written in Node.JS, and handles all communications from cuLearn. It logs in, downloads the HTML of the course list and the grades for each course, then parses that data into useful information, and saves it to the database.

## Android App
**Link to Android repository**: https://gitlab.com/JeremyKennedy/culearn-grades-android

The Android app is written in Java, and only communicates with the API server, never touching CuLearn. It receives the formatted courses and grades from the API, and then displays it to the user in an easy to read format. Other than requests for information, the only thing the app sends to the API server are what courses the user has activated. Since the app is fully functional, any questions regarding specifics of the implementation can be answered in the source code.

# User Interface

## Design
![](documentation/main.png)

The application was designed to look similar to Carleton’s theme and official colors, while remaining simple. The whole app uses a red theme, and features Carleton’s logo in the title.

## Course tabs
![](documentation/courses.png)

The application is navigated mainly using tabs. There’s easy and quick navigation between all your courses, and the list isn’t cluttered with courses you don’t want – it just shows the courses you select. These tabs are populated with the course code of each selected course (“BUSI 3401”) and navigation can be done through either tapping on a course, or swiping left and right on the grades display.

## Grade list
![](documentation/grades.png)

Underneath the tab bar, each course displays a list of grades. Each grade item displays the grade title, as well as the mark, range of marks, and percentage (if available).

## Refresh button
![](documentation/refresh.png)

The refresh button is located in the bottom right corner of the main activity. Pressing it will trigger a grades refresh, explained below. When the user scrolls, the button will animate out of view so that the user can read text that would normally be covered by the button. Once the user stops scrolling, after a few seconds the button will back come back into view.

## Information bar
![](documentation/snackbar.png)

Upon starting a refresh, either by opening the app or clicking the refresh button, the app will display an information bar at the bottom of the screen informing the user of the app’s current task. Since refreshes normally take a few seconds, providing this information to the user helps reduce confusion. This bar is referred to as a “Snackbar” in Android documentation.

# Functions

## Refresh on start
![](documentation/refresh-start.png)

Immediately after opening the app, students can instantly see their grades. In the background, a grade refresh is started, and after a few seconds the app will update itself with the latest information available from CuLearn. The user is kept informed as the app updates via the information bar.

## Manual refresh
Even though the application refreshes on start, users may want to force a refresh of their grades. This can be done by clicking the application’s refresh button. This will send a request to the API, which will retrieve and parse the relevant information from CuLearn. After the processing is complete, the API will respond to the app with the user’s courses and grades. The app will immediately update its display with this data. This whole process takes only a few seconds.

## Course activation settings
![](documentation/course-settings.png)

Clicking the “Courses” button on the main toolbar will bring the user to the above page. This is where they can select which courses they wish to see on the main tab bar. Users can review old grades, or activate new courses. Clicking on a course will immediately send a request to the API server telling it to activate or deactivate the course. A course’s activation status is what determines whether the API will fetch, store, and deliver the course’s grades. By having the API only process active courses, it has much less work to do, increasing the response time dramatically. Clicking the back arrow in the toolbar or pressing the devices’ back button will bring the user back to the main interface, where a refresh is immediately triggered to fetch the grades of the newly selected courses.

## Troubleshooting menu
![](documentation/troubleshooting.png)

The main toolbar has a menu button, and pressing it will display four troubleshooting options. Although normal use won’t need any of these functions, occasionally issues with the CuLearn authentication cookies stored on the server will require manual intervention with the “Login” option. This simply sends a request to the API asking it to force a re-login to CuLearn, which will fix any issues with courses and grades not refreshing properly.

The other options in this menu are very rarely used. “Get Courses” will fetch the courses and grades from the API, without it refreshing CuLearn at all. “Activate Courses” will automatically activate any courses from the current semester, which will only be useful twice per year when students start a new semester. “Get Grades” does the same thing as the main refresh button, and has the API refresh the latest grades from CuLearn.


# Roadmap
## Feedback
The first and most simple feature is to show feedback when a user taps a grade. Sometimes professors provide feedback when they mark assignments, and the API already processes and sends this data to the app. To implement this, all I have to do is display a dialog with the feedback when the user taps a grade.

## Custom credentials
Next is to support custom logins. Currently, the app uses a username hardcoded in the APK, which means users can only be changed by changing the source code and rebuilding the app. Luckily I have already implemented support for multiple users into the API, so the app would just need a dialog asking for a username and password, which is then sent to the API for processing.

## Custom labels
Right now, the course labels on the tab bar just use the course’s code, for example “BUSI 3401”. This feature would allow users to change the label to something more recognizable, such as “App Dev”. This could be implemented by adding an edit button to each course in the settings page, and adding another field to the Course class to keep track of the custom name.

## Web application
This feature is to offer a web application that users can sign into on any computer to quickly see their grades. This was working a few months ago, but after shifting my focus to the Android app, I eventually made enough changes to the API that the web application became nonfunctional. I could implement this again, but most people seem to prefer using the Android app anyway.

## Highlight updated courses
This feature idea is one of the most useful – highlighting courses with new grades. This is already implemented in the API, so I just need to decide the best way to display that information. One option would be to italicize or bold the course labels for updated courses, and clearing the style once the user views the course.

## Push notifications
The continuation of the previous idea is to send push notifications when new grades are received. This would involve the API continuously refreshing from cuLearn, which would be very difficult to implement properly, especially when multiple users are involved. However, out of all the features in the roadmap, I think this one would be the most useful and most appreciated by the users.