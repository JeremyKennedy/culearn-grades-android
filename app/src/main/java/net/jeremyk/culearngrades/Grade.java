package net.jeremyk.culearngrades;

class Grade {
    private final String title;
    private final String grade;
    private final String range;
    private final String percentage;
    private final String feedback;

    public Grade(String title, String grade, String range, String percentage, String feedback) {
        this.title = title;
        this.grade = grade;
        this.range = range;
        this.percentage = percentage;
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Grade{");
        sb.append("title='").append(getTitle()).append('\'');
        sb.append(", grade='").append(getGrade()).append('\'');
        sb.append(", range='").append(getRange()).append('\'');
        sb.append(", percentage='").append(getPercentage()).append('\'');
        sb.append(", feedback='").append(getFeedback()).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getTitle() {
        return title;
    }

    String getGrade() {
        return grade;
    }

    String getRange() {
        return range;
    }

    String getPercentage() {
        return percentage;
    }

    private String getFeedback() {
        return feedback;
    }
}
