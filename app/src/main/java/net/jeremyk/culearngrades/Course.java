package net.jeremyk.culearngrades;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Course {
    private ArrayList<Grade> gradesParsed;
    private String username;
    private String name;
    private int courseId;
    private boolean active;
    private String grades;
    private String lastFetched;
    private String lastChanged;

    public Course(String username, String name, int courseId, boolean active, String grades, ArrayList<Grade> gradesParsed, String lastFetched, String lastChanged) {
        this.setUsername(username);
        this.setName(name);
        this.setCourseId(courseId);
        this.setActive(active);
        this.setGrades(grades);
        this.setGradesParsed(gradesParsed);
        this.setLastFetched(lastFetched);
        this.setLastChanged(lastChanged);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Course{");
        sb.append("gradesParsed=").append(gradesParsed);
        sb.append(", username='").append(username).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", courseId=").append(courseId);
        sb.append(", active=").append(active);
        sb.append(", grades='").append(grades).append('\'');
        sb.append(", lastFetched='").append(lastFetched).append('\'');
        sb.append(", lastChanged='").append(lastChanged).append('\'');
        sb.append('}');
        return sb.toString();
    }

    String getFriendlyName() {
        Matcher m = Pattern.compile("([A-Z]{4}[0-9]{4})").matcher(name);
        String friendlyName = "NO MATCH";
        if (m.find()) {
            friendlyName = m.group(0).substring(0, 4) + " " + m.group(0).substring(4, 8);
        }
        return friendlyName;
    }

    ArrayList<Grade> getGradesParsed() {
        return gradesParsed;
    }

    private void setGradesParsed(ArrayList<Grade> gradesParsed) {
        this.gradesParsed = gradesParsed;
    }

    private void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    boolean isActive() {
        return active;
    }

    void setActive(boolean active) {
        CourseManager.updateCourseById(courseId, active);
        this.active = active;
    }

    private void setGrades(String grades) {
        this.grades = grades;
    }

    private void setLastFetched(String lastFetched) {
        this.lastFetched = lastFetched;
    }

    private void setLastChanged(String lastChanged) {
        this.lastChanged = lastChanged;
    }
}
