package net.jeremyk.culearngrades;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import static net.jeremyk.culearngrades.CourseManager.CourseList;

public class ActivatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activator_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView mListView = (ListView) findViewById(R.id.activator_list_view);
        CourseListArrayAdapter mAdapter = new CourseListArrayAdapter(this, CourseList);
        mListView.setAdapter(mAdapter);
    }

    private static class CourseListArrayAdapter extends ArrayAdapter<Course> {
        private final Context context;

        CourseListArrayAdapter(Context context, ArrayList<Course> courses) {
            super(context, R.layout.activator_list_item, courses);
            this.context = context;
        }

        @NonNull
        @Override
        public View getView(int position, View view, @NonNull ViewGroup parent) {
            final ViewHolder viewHolder;
            if (view == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.activator_list_item, parent, false);
                viewHolder.list_name = (TextView) view.findViewById(R.id.activator_list_name);
                viewHolder.list_active = (CheckBox) view.findViewById(R.id.activator_list_active);
                view.setTag(viewHolder);
                viewHolder.list_active.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int position = (int) view.getTag();
                        boolean newState = !CourseList.get(position).isActive();
                        CourseList.get(position).setActive(newState);
                    }
                });
                viewHolder.list_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int position = (int) view.getTag();
                        boolean newState = !CourseList.get(position).isActive();
                        CourseList.get(position).setActive(newState);
                        viewHolder.list_active.setChecked(newState);
                    }
                });
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            Course course = CourseList.get(position);
            viewHolder.list_name.setText(course.getName());
            viewHolder.list_active.setChecked(course.isActive());
            viewHolder.list_name.setTag(position);
            viewHolder.list_active.setTag(position);
            return view;
        }

        class ViewHolder {
            TextView list_name;
            CheckBox list_active;
        }
    }
}
