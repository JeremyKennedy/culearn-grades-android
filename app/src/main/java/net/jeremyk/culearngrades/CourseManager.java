package net.jeremyk.culearngrades;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import static net.jeremyk.culearngrades.MainActivity.USERNAME;
import static net.jeremyk.culearngrades.MainActivity.doSnackbar;
import static net.jeremyk.culearngrades.MainActivity.mSectionsPagerAdapter;

class CourseManager {
    private static final ApiService courseService = new Retrofit.Builder().baseUrl("http://my.jeremyk.net:13000/api/").addConverterFactory(
            GsonConverterFactory.create()).build().create(ApiService.class);
    static ArrayList<Course> CourseList = new ArrayList<>();

    static void enforceLogin() {
        courseService.enforceLogin(USERNAME).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d("CULEARN", "done enforceLogin: " + String.valueOf(response.body()));
                //TODO: make this return the current username
                refreshCourses();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("CULEARN", "error enforceLogin: " + String.valueOf(t));
                // TODO: handle error
            }
        });
    }

    // only needs to be used on first run of API
    private static void refreshCourses() {
        courseService.refreshCourses(USERNAME).enqueue(new Callback<List<Course>>() {
            @Override
            public void onResponse(Call<List<Course>> call, Response<List<Course>> response) {
                Log.d("CULEARN", "done refreshCourses: " + String.valueOf(response.body()));
                getCourses();
            }

            @Override
            public void onFailure(Call<List<Course>> call, Throwable t) {
                Log.d("CULEARN", "error refreshCourses: " + String.valueOf(t));
                // TODO: handle error
            }
        });
    }

    static void keywordActivate(String keyword) {
        Log.d("CULEARN", "starting keywordActivate: " + USERNAME + keyword);
        courseService.keywordActivate(USERNAME, keyword).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d("CULEARN", "done keywordActivate: " + String.valueOf(response.body()));
                getCourses();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("CULEARN", "error keywordActivate: " + String.valueOf(t));
                // TODO: handle error
            }
        });
    }

    static void refreshGradesActive() {
        courseService.refreshGradesActive(USERNAME).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d("CULEARN", "done refreshGradesActive: " + String.valueOf(response.body()));
                doSnackbar = true;
                getCourses();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("CULEARN", "error refreshGradesActive: " + String.valueOf(t));
                // TODO: handle error
            }
        });
    }

    // does not interact with cuLearn, just the API
    static void getCourses() {
        courseService.getCoursesByUser(USERNAME).enqueue(new Callback<List<Course>>() {
            @Override
            public void onResponse(Call<List<Course>> call, Response<List<Course>> response) {
                Log.d("CULEARN", "done getCoursesByUser: " + String.valueOf(response.body()));
                CourseList.clear();
                CourseList.addAll(response.body());
                mSectionsPagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Course>> call, Throwable t) {
                Log.d("CULEARN", "error getCoursesByUser: " + String.valueOf(t));
                // TODO: handle error
            }
        });
    }

    static void updateCourseById(int courseId, boolean active) {
        Log.d("CULEARN", "starting updateCourseById: " + courseId + active);
        courseService.updateCourseById("{\"courseId\":\"" + courseId + "\"}", active).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d("CULEARN", "done updateCourseById: " + String.valueOf(response.body()));
                getCourses();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("CULEARN", "error updateCourseById: " + String.valueOf(t));
                // TODO: handle error
            }
        });
    }

    static ArrayList<Course> getActiveCourses() {
        ArrayList<Course> activeCourses = new ArrayList<>();
        for (Course course : CourseList) {
            if (course.isActive()) activeCourses.add(course);
        }
        return activeCourses;
    }

    @SuppressWarnings("SameParameterValue")
    interface ApiService {
        @POST("students/enforceLogin")
        Call<Object> enforceLogin(@Query("username") String username);

        @POST("courses/refreshCourses")
        Call<List<Course>> refreshCourses(@Query("username") String username);

        @POST("courses/keywordActivate")
        Call<Object> keywordActivate(@Query("username") String username, @Query("keyword") String keyword);

        @POST("courses/refreshGradesActive")
        Call<Object> refreshGradesActive(@Query("username") String username);

        @GET("courses/?filter=")
        Call<List<Course>> getCoursesByUser(@Query("[where][username]") String username);

        @FormUrlEncoded
        @POST("courses/update")
        Call<Object> updateCourseById(@Query("where") String courseId, @Field("active") boolean active);
    }
}
