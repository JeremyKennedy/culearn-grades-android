package net.jeremyk.culearngrades;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import static net.jeremyk.culearngrades.R.id.container;

public class MainActivity extends AppCompatActivity {

    //    public static final String USERNAME = "jeremykennedy";
    public static final String USERNAME = "kaylagrooms";
    public static boolean doSnackbar = false;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    public static SectionsPagerAdapter mSectionsPagerAdapter;

    public MainActivity() throws IOException {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        // TODO: fix list being too tall for the screen

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setIcon(R.mipmap.culearn);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        /*
      The {@link ViewPager} that will host the section contents.
     */
        ViewPager mViewPager = (ViewPager) findViewById(container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displaySnackbar(getString(R.string.notify_updating), false);
                CourseManager.refreshGradesActive();
            }
        });
        displaySnackbar(getString(R.string.notify_updating), false);
        CourseManager.getCourses();
        CourseManager.refreshGradesActive();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(), ActivatorActivity.class);
            startActivity(i);
        }
        if (id == R.id.action_enforce_login) {
            CourseManager.enforceLogin();
        }
        if (id == R.id.action_get_courses) {
            CourseManager.getCourses();
        }
        if (id == R.id.action_activate_2017) {
            displaySnackbar("Activating 2017 courses,,,", false);
            CourseManager.keywordActivate("2017");
        }
        if (id == R.id.action_refresh_grades_active) {
            CourseManager.refreshGradesActive();
        }
        // TODO: show snackbar for all of these

        return super.onOptionsItemSelected(item);
    }

    private void displaySnackbar(String text, boolean verify) {
        View view = findViewById(R.id.main_content);
        if (verify) {
            if (doSnackbar) {
                doSnackbar = false;
                Snackbar.make(view, text, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        } else Snackbar.make(view, text, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    public static class GradeViewFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public GradeViewFragment() {
        }

        public static GradeViewFragment newInstance(int sectionNumber) {
            GradeViewFragment fragment = new GradeViewFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.main_fragment, container, false);
            int position = getArguments().getInt(ARG_SECTION_NUMBER);
            ArrayList<Grade> grades;
            try {
                grades = CourseManager.getActiveCourses().get(position).getGradesParsed();
            } catch (IndexOutOfBoundsException e) {
                grades = new ArrayList<>();
            }
            ListView mListView = (ListView) rootView.findViewById(R.id.grade_list_view);
            mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
                    int btn_initPosY = fab.getScrollY();
                    if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                        fab.animate().setStartDelay(0).setDuration(75).translationYBy(350);
                    } else {
                        fab.animate().setStartDelay(500).setDuration(300).translationY(btn_initPosY);
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                }
            });
            mListView.setAdapter(new GradeListArrayAdapter(getContext(), grades));
            return rootView;
        }
        // TODO: add pull to refresh
    }

    private static class GradeListArrayAdapter extends ArrayAdapter<Grade> {
        private final Context context;
        private final ArrayList<Grade> grades;

        GradeListArrayAdapter(Context context, ArrayList<Grade> grades) {
            super(context, R.layout.main_list_item, grades);
            this.context = context;
            this.grades = grades;
        }

        @NonNull
        @Override
        public View getView(int position, View view, @NonNull ViewGroup parent) {
            ViewHolder viewHolder;
            if (view == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.main_list_item, parent, false);
                viewHolder.list_title = (TextView) view.findViewById(R.id.list_title);
                viewHolder.list_grade = (TextView) view.findViewById(R.id.list_grade);
                viewHolder.list_range = (TextView) view.findViewById(R.id.list_range);
                viewHolder.list_percent = (TextView) view.findViewById(R.id.list_percent);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.list_title.setText(grades.get(position).getTitle());
            viewHolder.list_grade.setText(grades.get(position).getGrade());
            viewHolder.list_range.setText(grades.get(position).getRange());
            viewHolder.list_percent.setText(grades.get(position).getPercentage());
            return view;
            // TODO: display feedback on tap, and visual cue showing feedback is available
        }

        class ViewHolder {
            TextView list_title;
            TextView list_grade;
            TextView list_range;
            TextView list_percent;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the pages.
     */
    @SuppressWarnings("WeakerAccess")
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
            displaySnackbar(getString(R.string.notify_complete), true);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return GradeViewFragment.newInstance(position);
        }

        @Override
        public int getItemPosition(Object item) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            try {
                int size = CourseManager.getActiveCourses().size();
                return size > 0 ? size : 1;
            } catch (IndexOutOfBoundsException e) {
                return 1;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            try {
                return CourseManager.getActiveCourses().get(position).getFriendlyName();
            } catch (IndexOutOfBoundsException e) {
                return "No courses active yet.";
            }
        }
    }
}
